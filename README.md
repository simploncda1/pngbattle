# README - Application de Concours de Dessin

Ce README fournit des informations essentielles sur le développement de l'application "Concours de Dessin". L'application permet aux utilisateurs de participer à des concours de dessin en soumettant leurs créations en fonction d'un thème hebdomadaire. Les utilisateurs peuvent liker les dessins, et les deux dessins les plus populaires s'affronteront à la fin de chaque semaine. Le dessin gagnant est ensuite affiché sur la page d'accueil.

## Table des matières

- [Diagrammes de Use Case](#diagrammes-de-use-case)
- [Maquette fonctionnelle navigable](#maquette-fonctionnelle-navigable)
- [Diagramme d'entité](#diagramme-dentité)
- [Script SQL de mise en place de la BDD](#script-sql-de-mise-en-place-de-la-bdd)
- [Projet backend](#projet-backend)

## Diagrammes de Use Case

![Diagramme de Use Case](./images/useCase.png)

Ce diagramme de Use Case représente les interactions entre les acteurs (utilisateurs) et le système, décrivant les fonctionnalités de l'application. Il identifie les cas d'utilisation tels que l'inscription, la soumission de dessins, la notation des dessins, etc.

## Maquette fonctionnelle navigable

La maquette fonctionnelle navigable de l'application a été créée à l'aide de Figma. Elle vous permettra de visualiser le design et la disposition des différentes pages de l'application. Pour accéder à la maquette, [cliquez ici](https://shattereddisk.github.io/rickroll/rickroll.mp4).

## Diagramme d'entité

![Diagramme d'entité](./images/Entities.png)

Le diagramme d'entité représente la structure de la base de données de l'application. Il décrit les entités telles que les utilisateurs, les dessins, les concours, les votes, etc., ainsi que leurs relations.

## Script SQL de mise en place de la BDD

Le script SQL permet de créer la base de données et d'initialiser un jeu de données de test. Vous pouvez le trouver dans le fichier `/src/main/resources/database.sql` du dépôt GitLab/GitHub.

## Projet backend

Le projet backend de l'application a été développé en utilisant le langage Java avec JDBC (Java Database Connectivity) pour interagir avec la base de données.

## Maquette fonctionnelle navigable

La maquette fonctionnelle navigable de l'application a été créée à l'aide de Figma. Elle vous permettra de visualiser le design et la disposition des différentes pages de l'application. Pour accéder à la maquette, [cliquez ici](https://www.figma.com/proto/aInnAO6ckqBqoEcJ0KpP2n/PNGBattle?type=design&node-id=1-2&t=GQnUqvdAo9uyAwhO-0&scaling=min-zoom&page-id=0%3A1&starting-point-node-id=1%3A2).

## SonarQube

SonarQube est un outil de qualité de code qui permet d'analyser et de mesurer la qualité de votre code source. Il détecte les bugs, les vulnérabilités et les mauvaises pratiques de codage. 

### Lancer SonarQube

```sh
    docker run -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube
```

### Créer un Token

- Ajouter le token dans `settings.xml` :

```xml
    <sonar.login>TOKEN</sonar.login>
```

### Lancer les tests et l'analyse SonarQube

1. Lancer les tests :

```sh
    mvn test
```

2. Lancer SonarQube :

```sh
    mvn sonar:sonar -s .m2/settings.xml
```