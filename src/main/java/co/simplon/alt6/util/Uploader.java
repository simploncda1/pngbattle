package co.simplon.alt6.util;

import org.springframework.stereotype.Component;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class Uploader {

    private final Environment env;

    public Uploader(Environment env) {
        this.env = env;
    }

    public String upload(String userName, MultipartFile filename2) throws IOException {
        // On stocke le chemin vers le dossier où on sauvegardera nos fichiers
        String pathToUploads = env.getProperty("kernel.project_dir") + "/public/uploads/";
    
        // On crée un dossier avec le nom de l'utilisateur s'il n'existe pas
        // FIXME: sécurité / bonne pratique ???
        String userFolderPath = pathToUploads + userName + "/";
        File userFolder = new File(userFolderPath);
        if (!userFolder.exists()) {
            userFolder.mkdirs();
        }
    
        // On génère un nom de fichier unique (il serait peut-être préférable de
        // récupérer l'extension d'une manière ou d'une autre)
        String filename = System.currentTimeMillis() + ".png";
    
        // On sauvegarde l'image originale dans le dossier de l'utilisateur
        Path originalPath = Paths.get(userFolderPath + filename);
        Files.copy(filename2.getInputStream(), originalPath);
    
        return filename;
    }
    
}
