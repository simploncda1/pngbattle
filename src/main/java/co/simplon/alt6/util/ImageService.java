package co.simplon.alt6.util;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class ImageService {

    private final Uploader uploader;

    public ImageService(Uploader uploader) {
        this.uploader = uploader;
    }

    public String uploadBase64Image(String userName, String base64Image) throws IOException {
        // Récupérer le type de l'image depuis la chaîne base64
        String[] parts = base64Image.split(",");
        String imageType = parts[0].split("/")[1].split(";")[0];

        // TODO: PNG ONLY
        // Générer un nom de fichier unique
        String filename =System.currentTimeMillis() + "." + imageType;

        // Décodez la chaîne base64 en bytes
        byte[] imageBytes = java.util.Base64.getDecoder().decode(parts[1]);

        // Créez un objet MultipartFile temporaire à partir des bytes décodés
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "image/" + imageType);
        MultipartFile multipartFile = new MyMultipartFile(filename, imageBytes, headers);

        // Utilisez la classe Uploader pour uploader l'image
        String uploadedFilename = uploader.upload(userName, multipartFile);

        return uploadedFilename;
    }

    // Implémentation personnalisée de MultipartFile
    private static class MyMultipartFile implements MultipartFile {
        private final String filename;
        private final byte[] bytes;
        private final MultiValueMap<String, String> headers;

        public MyMultipartFile(String filename, byte[] bytes, MultiValueMap<String, String> headers) {
            this.filename = filename;
            this.bytes = bytes;
            this.headers = headers;
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getOriginalFilename() {
            return filename;
        }

        @Override
        public String getContentType() {
            return headers.getFirst("Content-Type");
        }

        @Override
        public boolean isEmpty() {
            return bytes == null || bytes.length == 0;
        }

        @Override
        public long getSize() {
            return bytes.length;
        }

        @Override
        public byte[] getBytes() {
            return bytes;
        }

        @Override
        public InputStream getInputStream() {
            return new ByteArrayInputStream(bytes);
        }

        @Override
        public void transferTo(File dest) throws IOException, IllegalStateException {
            StreamUtils.copy(bytes, new FileOutputStream(dest));
        }
    }
}