package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.dao.interfaces.ContestDao;
import co.simplon.alt6.dao.interfaces.RoundDao;
import co.simplon.alt6.entities.Contest;
import co.simplon.alt6.entities.Round;

@Repository
public class RoundDaoImpl extends AbstractDao<Round> implements RoundDao {

    private final ContestDao contestDao;

    @Autowired
    private DataSource dataSource;

    public RoundDaoImpl(ContestDao contestDao) {
        super(
                "INSERT INTO round (phase,max_participation,contest_id) VALUES (?,?,?)",
                "SELECT * FROM round",
                "SELECT * FROM round WHERE id=?",
                "UPDATE round SET phase=?,max_participation=?,contest_id=? WHERE id=?",
                "DELETE FROM round WHERE id=?");

        this.contestDao = contestDao;
    }

    @Override
    public Contest getAssociatedContest(int id) {
        return contestDao.getById(id);
    }

    @Override
    protected Round sqlToEntity(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String phase = rs.getString("phase");
        int maxParticipation = rs.getInt("max_participation");

        Contest contest = getAssociatedContest(rs.getInt("contest_id"));

        return new Round(id, phase, maxParticipation, contest);
    }

    @Override
    protected void setEntityId(Round entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Round entity) throws SQLException {
        stmt.setString(1, entity.getPhase());
        stmt.setInt(2, entity.getMaxParticipation());
        stmt.setInt(3, entity.getContest().getId());
        // stmt.setInt(3, entity.getContestId());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Round entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(4, entity.getId());
    }

    @Override
    public boolean delete(int roundId) {
        try (Connection connection = dataSource.getConnection()) {

            // Suppression des likes liés au round (null)
            String updateLikesSQL = "UPDATE likes SET round_id = NULL WHERE round_id IN (SELECT id FROM round WHERE contest_id = ?)";
            try (PreparedStatement updateLikesStmt = connection.prepareStatement(updateLikesSQL)) {
                updateLikesStmt.setInt(1, roundId);
                int rowsUpdated = updateLikesStmt.executeUpdate();
                if (rowsUpdated <= 0) {
                    connection.rollback();
                    return false;
                }
            }

            // Suppression des rounds liés au contest
            String deleteRoundSQL = "DELETE FROM round WHERE contest_id = ?";
            try (PreparedStatement deleteRoundStmt = connection.prepareStatement(deleteRoundSQL)) {
                deleteRoundStmt.setInt(1, roundId);
                int rowsDeleted = deleteRoundStmt.executeUpdate();
                if (rowsDeleted <= 0) {
                    connection.rollback();
                    return false;
                }
            }

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Récupère la liste des Rounds associés à un Contest spécifié par son ID.
     * Chaque round est représenté en tant qu'objet Round, sans les détails du
     * Contest associé.
     *
     * @param id L'ID du Contest pour lequel récupérer les Rounds.
     * @return Une liste d'objets Round représentant les Rounds associés au
     *         Contest.
     */
    public List<Round> getAllRoundByContestId(int id) {
        List<Round> rounds = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            String SQL = "SELECT * FROM round WHERE contest_id = ?";
            try (PreparedStatement getStmt = connection.prepareStatement(SQL)) {
                getStmt.setInt(1, id);

                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        int roundId = rs.getInt("id");
                        String phase = rs.getString("phase");
                        int maxParticipation = rs.getInt("max_participation");

                        /**
                         * Si on veux afficher les contest dans via /api/contest -> Rounds ->
                         * Contest{...}
                         * 
                         * Contest contest = getAssociatedContest(rs.getInt("contest_id"));
                         * Round round = new Round(roundId, phase, maxParticipation, contest);
                         */

                        Round round = new Round(roundId, phase, maxParticipation);

                        rounds.add(round);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rounds;
    }

}
