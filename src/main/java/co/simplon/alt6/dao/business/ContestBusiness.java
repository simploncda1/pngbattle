package co.simplon.alt6.dao.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplon.alt6.dao.ContestDaoImpl;
import co.simplon.alt6.dao.RoundDaoImpl;
import co.simplon.alt6.entities.Contest;
import co.simplon.alt6.entities.Round;

@Service
public class ContestBusiness {

    @Autowired
    private ContestDaoImpl contestDao;
    @Autowired
    private RoundDaoImpl roundDao;

    public ContestBusiness(ContestDaoImpl contestDao, RoundDaoImpl roundDao) {
        this.contestDao = contestDao;
        this.roundDao = roundDao;
    }

    public ContestBusiness(ContestDaoImpl contestDao) {
        this.contestDao = contestDao;
    }

    public ContestBusiness() {
    }

    /**
     * Récupère un Contest avec tous les Rounds associés à partir de son ID.
     *
     * @param id L'ID du Contest à récupérer.
     * @return Un objet Contest avec ses Rounds associés.
     */
    public Contest getContestWithRounds(int id) {
        Contest contest = contestDao.getById(id);
        List<Round> rounds = contestDao.getAllRoundByContestId(id);
        contest.setRounds(rounds);
        return contest;
    }

    public Contest getFinalRound() {
        return contestDao.getFinalRound();
    }

    public Contest getPreviousRound() {
        return contestDao.getPreviousRound();
    }

    public Contest getActualRound() {
        return contestDao.getActualRound();
    }

    public Contest getNextRound() {
        return contestDao.getNextRound();
    }

    // Fait en SQL

    // /**
    //  * Crée un Contest avec des Rounds prédéfinis.
    //  *
    //  * @param contest Le Contest à créer avec les Rounds.
    //  * @return Le Contest créé avec ses Rounds associés.
    //  */
    // public Contest createContestWithRounds(Contest contest) {
    //     contestDao.add(contest);
    //     Contest addedContest = contestDao.getById(contest.getId());
    //     createRoundsForContest(addedContest);
    //     addedContest.setRounds(roundDao.getAllRoundByContestId(addedContest.getId()));
    //     return addedContest;
    // }

    // /**
    //  * Crée des Rounds prédéfinis pour un Contest.
    //  *
    //  * @param contest Le Contest pour lequel créer les Rounds.
    //  */
    // private void createRoundsForContest(Contest contest) {
    //     Round round1 = new Round("Qualification", 50, contest);
    //     Round round2 = new Round("Finals", 2, contest);
    //     Round round3 = new Round("Winner", 1, contest);

    //     roundDao.add(round1);
    //     roundDao.add(round2);
    //     roundDao.add(round3);
    // }

    public List<Contest> getAll() {
        List<Contest> contests = contestDao.getAll();

        // Pour chaque concours, récupère et associe les tours.
        for (Contest contest : contests) {
            List<Round> rounds = roundDao.getAllRoundByContestId(contest.getId());
            contest.setRounds(rounds);
        }

        return contests;
    }

    public Boolean add(Contest contest) {
        return contestDao.add(contest);
    }

}
