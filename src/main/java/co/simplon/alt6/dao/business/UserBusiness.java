package co.simplon.alt6.dao.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplon.alt6.dao.PaintingDaoImpl;
import co.simplon.alt6.dao.UserDaoImpl;
import co.simplon.alt6.entities.Painting;
import co.simplon.alt6.entities.User;

@Service
public class UserBusiness {

    @Autowired
    UserDaoImpl repo;

    @Autowired
    PaintingDaoImpl paintingDao;

    public UserBusiness() {
    }

    public UserBusiness(UserDaoImpl repo) {
        this.repo = repo;
    }

    public User getOne(int id) {
        User user = repo.getById(id);
        List<Painting> paintings = paintingDao.getPaitingByUserId(id);

        user.setEmail(null);
        user.setGender(null);
        user.setBirthdate(null);        
        user.setPassword(null);

        user.setPaintings(paintings); 

        return user;
    }

    public List<User> getAll() {
        List<User> users = repo.getAll();
        for (User user : users) {
            user.setPassword(null);
        }
        return users;
    }

}
