package co.simplon.alt6.dao.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import co.simplon.alt6.dao.LikesDaoImpl;
import co.simplon.alt6.dao.exception.UserAlreadyLikedException;
import co.simplon.alt6.dao.interfaces.UserLikesDao;
import co.simplon.alt6.entities.Likes;
import co.simplon.alt6.entities.User;
import co.simplon.alt6.entities.UserLikes;

@Service
public class LikesBusiness {

    @Autowired
    private LikesDaoImpl likesDao;

    @Autowired
    private UserLikesDao userLikesDao;

    public LikesBusiness() {
    }

    public LikesBusiness(LikesDaoImpl repo) {
        this.likesDao = repo;
    }

    public List<Likes> getLikesWithRounds() {
        return likesDao.getLikesWithRounds();
    }

    public List<Likes> getLikesByRoundId(int id) {
        return likesDao.getLikesByRoundId(id);
    }

    public Likes likesPictures(@AuthenticationPrincipal User user, int painting_id, int round_id) {

        Likes likes = new Likes();
        likes.setId(painting_id);

        List<UserLikes> userLikesList = userLikesDao.getLikesByUserId(user.getId());

        // vérifie si l'user à like un tableau dans un round
        Boolean isAlreadyLiked = likesDao.hasUserLikedPaintingForRound(user.getId(), round_id);

        if (!userLikesList.isEmpty()) {
            for (UserLikes userLikes : userLikesList) {
                // Supprime le like si c'est le même tableau
                if (userLikes.getLikesId() == painting_id) {
                    userLikesDao.deleteUserLikesByIdAndLikes(user.getId(), painting_id);
                    return likesDao.unlikesPictures(likes);
                }
            }
            // Si c'est le même round
            if (isAlreadyLiked) {
                throw new UserAlreadyLikedException("User already liked for this round");
            }
        }
        userLikesDao.add(new UserLikes(user.getId(), painting_id));
        return likesDao.likesPictures(likes);
    }

    public Likes unlikesPictures(int id) {
        Likes likes = new Likes();
        likes.setId(id);
        return likesDao.unlikesPictures(likes);
    }

    public Boolean isPaintingLiked(User user, int painting_id, int round_id) {
        return likesDao.isPaintingLiked(user.getId(), painting_id, round_id);
    }

}
