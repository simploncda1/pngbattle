package co.simplon.alt6.dao.business;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import co.simplon.alt6.dao.ContestDaoImpl;
import co.simplon.alt6.dao.LikesDaoImpl;
import co.simplon.alt6.dao.PaintingDaoImpl;
import co.simplon.alt6.dao.RoundDaoImpl;
import co.simplon.alt6.dao.exception.UserAlreadyPostedException;
import co.simplon.alt6.entities.Contest;
import co.simplon.alt6.entities.Likes;
import co.simplon.alt6.entities.Painting;
import co.simplon.alt6.entities.Round;
import co.simplon.alt6.entities.User;
import co.simplon.alt6.util.ImageService;

@Service
public class PaintingBusiness {

    @Autowired
    private PaintingDaoImpl repo;

    @Autowired
    private LikesDaoImpl likesDao;

    @Autowired
    private RoundDaoImpl roundDao;

    @Autowired
    private ContestDaoImpl contestDao;

    @Autowired
    private ImageService uploader;

    public PaintingBusiness() {
    }

    public PaintingBusiness(PaintingDaoImpl repo) {
        this.repo = repo;
    }

    // Faire que ça poste une image directement sur le serveur ?

    /**
     * Poste une peinture sur le contest actuel.
     *
     * @param user     L'utilisateur authentifié qui soumet la peinture.
     * @param painting La peinture à soumettre.
     * @return true si la soumission réussit, sinon false.
     * @throws Exception Si une erreur survient pendant le processus.
     */
    public boolean postPainting(@AuthenticationPrincipal User user, Painting painting) throws Exception {

        Contest actualContest = contestDao.getActualRound();
        int idRoundFromActuelContest = actualContest.getRounds().get(0).getId();

        Round round = roundDao.getById(idRoundFromActuelContest);
        List<Likes> participation = likesDao.getLikesByRoundId(idRoundFromActuelContest);

        int possibleParticipation = participation.size();
        int maxParticipation = round.getMaxParticipation();

        if (possibleParticipation >= maxParticipation) {
            throw new Exception("Le nombre maximum de participation est atteinte.");
        }

        if (!round.getPhase().equals("Qualification")) {
            throw new Exception(
                    "Il est uniquement possible de soumettre sur un tour en phase '" + round.getPhase() + "'.");
        }

        if (repo.isUserAlreadyPosted(idRoundFromActuelContest, user.getId())) {
            throw new UserAlreadyPostedException("L'utilisateur a déjà posté une œuvre pour ce tour.");
        }

        Likes likes = new Likes(0, round, painting);
        int user_id = user.getId();

        painting.setDate(LocalDate.now());
        painting.setUser_id(user_id);

        String userName =  user.getFirst_name() + user.getName();

        String imagePath = uploader.uploadBase64Image(userName,painting.getPicture());

        painting.setPicture(imagePath);

        Boolean paintSuccess = repo.add(painting);
        likesDao.add(likes);

        return paintSuccess;
    }

    public Painting getOne(int id) {
        return repo.getById(id);
    }

    public List<Painting> getAll() {
        List<Painting> paintings = repo.getAll();
        return paintings;
    }

}
