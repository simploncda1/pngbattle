package co.simplon.alt6.dao.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import co.simplon.alt6.dao.UserDaoImpl;
import co.simplon.alt6.dao.exception.UserAlreadyExistsException;
import co.simplon.alt6.entities.User;


@Service
public class AuthBusiness{
    @Autowired
    private PasswordEncoder hasher;
    @Autowired
    private UserDaoImpl userRepo;

    public void register(User user) throws Exception {
        Optional<User> existingUser = userRepo.getByEmail(user.getEmail());

        if(existingUser.isPresent()) {
            throw new UserAlreadyExistsException("User already exist");
        }

        String hashedPassword = hasher.encode(user.getPassword());
        user.setPassword(hashedPassword);
        // user.setRole("ROLE_USER");
        userRepo.add(user);
    }

}
