package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.dao.interfaces.PaintingDao;
import co.simplon.alt6.entities.Likes;
import co.simplon.alt6.entities.Painting;

@Repository
public class PaintingDaoImpl extends AbstractDao<Painting> implements PaintingDao {

    @Autowired
    private DataSource dataSource;

    public PaintingDaoImpl() {
        super(
                "INSERT INTO painting (title,description,picture,date,user_id) VALUES (?,?,?,?,?)",
                "SELECT * FROM painting",
                "SELECT * FROM painting WHERE id=?",
                "UPDATE painting SET title=?,description=?,picture=?,date=?,user_id=? WHERE id=?",
                "DELETE FROM likes WHERE id=?");
    }

    @Override
    protected Painting sqlToEntity(ResultSet rs) throws SQLException {
        return new Painting(
                rs.getInt("id"),
                rs.getString("title"),
                rs.getString("description"),
                rs.getString("picture"),
                rs.getDate("date").toLocalDate(),
                rs.getInt("user_id"));
    }

    @Override
    protected void setEntityId(Painting entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Painting entity) throws SQLException {
        stmt.setString(1, entity.getTitle());
        stmt.setString(2, entity.getDescription());
        stmt.setString(3, entity.getPicture());
        stmt.setDate(4, java.sql.Date.valueOf(entity.getDate()));
        stmt.setInt(5, entity.getUser_id());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Painting entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(6, entity.getId());
    }

    @Override
    public List<Painting> getPaitingByUserId(int id) {
        List<Painting> paintings = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            String SQL = "SELECT p.*, l.* FROM painting p JOIN likes l on p.id = l.painting_id WHERE user_id = ?";
            try (PreparedStatement getStmt = connection.prepareStatement(SQL)) {
                getStmt.setInt(1, id);

                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        Painting painting = new Painting(
                                rs.getInt("p.id"),
                                rs.getString("p.title"),
                                rs.getString("p.description"),
                                rs.getString("p.picture"),
                                rs.getDate("p.date").toLocalDate(),
                                rs.getInt("p.user_id"));

                        Likes likes = new Likes();
                        likes.setNumber(rs.getInt("l.number"));

                        painting.setLikes(likes);

                        paintings.add(painting);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return paintings;
    }

    /**
     * Vérifie si un utilisateur a déjà soumis une peinture pour un tour spécifié.
     *
     * @param round_id L'identifiant du tour pour lequel la soumission est vérifiée.
     * @param user_id  L'identifiant de l'utilisateur dont la soumission est
     *                 recherchée.
     * @return true si l'utilisateur a déjà soumis une peinture pour le tour, sinon
     *         false.
     * @throws SQLException En cas d'erreur lors de l'accès à la base de données.
     */
    @Override
    public Boolean isUserAlreadyPosted(int round_id, int user_id) {

        try (Connection connection = dataSource.getConnection()) {
            String SQL = "SELECT * FROM painting JOIN likes WHERE likes.round_id = ? AND painting.user_id = ?";
            try (PreparedStatement getStmt = connection.prepareStatement(SQL)) {
                getStmt.setInt(1, round_id);
                getStmt.setInt(2, user_id);

                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }
}
