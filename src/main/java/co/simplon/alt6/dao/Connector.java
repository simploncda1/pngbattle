package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import co.simplon.alt6.util.AppProperties;

public class Connector {

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(AppProperties.getInstance().getProps("DATABASE_URL"));
    }
}
