package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.dao.interfaces.LikesDao;
import co.simplon.alt6.entities.Likes;
import co.simplon.alt6.entities.Painting;
import co.simplon.alt6.entities.Round;

@Repository
public class LikesDaoImpl extends AbstractDao<Likes> implements LikesDao {

    private RoundDaoImpl roundDao;
    private PaintingDaoImpl paintingDao;

    @Autowired
    private DataSource dataSource;

    public LikesDaoImpl(RoundDaoImpl roundDao, PaintingDaoImpl paintingDao) {
        super(
                "INSERT INTO likes (number,round_id,painting_id) VALUES (?,?,?)",
                "SELECT * FROM likes",
                "SELECT * FROM likes WHERE id=?",
                "UPDATE likes SET number=?,round_id=?,painting_id=? WHERE id=?",
                "DELETE FROM likes WHERE id=?");

        this.roundDao = roundDao;
        this.paintingDao = paintingDao;
    }

    @Override
    protected Likes sqlToEntity(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        int number = rs.getInt("number");

        Round round = getAssociatedRound(rs.getInt("round_id"));
        Painting painting = getAssociatedPainting(rs.getInt("painting_id"));

        return new Likes(id, number, round, painting);
        // return new Likes(
        // rs.getInt("id"),
        // rs.getInt("number"),
        // rs.getInt("round_id"),
        // rs.getInt("painting_id"));
    }

    private Painting getAssociatedPainting(int id) {
        return paintingDao.getById(id);
    }

    private Round getAssociatedRound(int id) {
        return roundDao.getById(id);
    }

    @Override
    protected void setEntityId(Likes entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Likes entity) throws SQLException {
        stmt.setInt(1, entity.getNumber());
        stmt.setInt(2, entity.getRound().getId());
        stmt.setInt(3, entity.getPainting().getId());
        // stmt.setInt(2, entity.getRoundId());
        // stmt.setInt(3, entity.getPaintingId());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Likes entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(4, entity.getId());
    }

    /**
     * Retrieves a list of Likes along with associated Round information.
     * This method performs a SQL join between the 'likes' and 'round' tables.
     * 
     * @return List of Likes with associated Round details
     */
    @Override
    public List<Likes> getLikesWithRounds() {
        List<Likes> likesList = new ArrayList<>();

        String SQL = "SELECT l.id, l.number, l.round_id, l.painting_id, r.phase, r.max_participation "
                + "FROM likes l "
                + "JOIN round r ON l.round_id = r.id";

        try (Connection connection = dataSource.getConnection();
                PreparedStatement getStmt = connection.prepareStatement(SQL);
                ResultSet rs = getStmt.executeQuery()) {

            while (rs.next()) {
                Likes likes = sqlToEntity(rs);

                Round round = new Round();
                round.setId(rs.getInt("round_id"));
                round.setPhase(rs.getString("phase"));
                round.setMaxParticipation(rs.getInt("max_participation"));

                likes.setRound(round);

                likesList.add(likes);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return likesList;
    }

    /**
     * Retrieves a list of Likes associated with a specific Round.
     * 
     * @param id The ID of the Round for which Likes are to be retrieved
     * @return List of Likes associated with the specified Round
     */
    @Override
    public List<Likes> getLikesByRoundId(int id) {
        List<Likes> likesList = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            String SQL = "SELECT * FROM likes WHERE round_id = ?";
            try (PreparedStatement getStmt = connection.prepareStatement(SQL)) {
                getStmt.setInt(1, id);

                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        int likesId = rs.getInt("id");
                        int number = rs.getInt("number");

                        Round round = getAssociatedRound(rs.getInt("round_id"));
                        Painting painting = getAssociatedPainting(rs.getInt("painting_id"));

                        Likes likes = new Likes(likesId, number, round, painting);
                        likesList.add(likes);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return likesList;
    }

    /**
     * Increases the number of likes for a picture by incrementing it.
     * 
     * @param likes The Likes object containing information about the likes.
     * @return An updated Likes object after incrementing the like count
     */
    @Override
    public Likes likesPictures(Likes likes) {
        Likes updatedLikes = null;

        try (Connection connection = dataSource.getConnection()) {
            String selectSQL = "SELECT * FROM likes WHERE id = ?";
            String updateSQL = "UPDATE likes SET number = number + 1 WHERE id = ?";

            try (
                    PreparedStatement selectStmt = connection.prepareStatement(selectSQL);
                    PreparedStatement updateStmt = connection.prepareStatement(updateSQL);) {

                connection.setAutoCommit(false);

                // Select Likes information
                selectStmt.setInt(1, likes.getId());
                try (ResultSet rs = selectStmt.executeQuery()) {
                    if (rs.next()) {

                        int likesId = rs.getInt("id");
                        Round round = getAssociatedRound(rs.getInt("round_id"));
                        Painting painting = getAssociatedPainting(rs.getInt("painting_id"));

                        // Update Likes number
                        updateStmt.setInt(1, likes.getId());
                        updateStmt.executeUpdate();

                        selectStmt.setInt(1, likes.getId());
                        try (ResultSet result = selectStmt.executeQuery()) {
                            if (result.next()) {
                                // Create a new Likes instance with updated values
                                updatedLikes = new Likes(likesId, result.getInt("number"), round, painting);
                            }
                        }

                    }
                }

                connection.commit();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return updatedLikes;
    }

    /**
     * Updates the number of likes for a picture by decrementing it.
     * 
     * @param likes The Likes object containing information about the likes.
     * @return An updated Likes object after decrementing the like count
     */
    @Override
    public Likes unlikesPictures(Likes likes) {
        Likes updatedLikes = null;

        try (Connection connection = dataSource.getConnection()) {
            String selectSQL = "SELECT * FROM likes WHERE id = ?";
            String updateSQL = "UPDATE likes SET number = number - 1 WHERE id = ?";

            try (
                    PreparedStatement selectStmt = connection.prepareStatement(selectSQL);
                    PreparedStatement updateStmt = connection.prepareStatement(updateSQL)) {

                connection.setAutoCommit(false);

                // Select Likes information
                selectStmt.setInt(1, likes.getId());
                try (ResultSet rs = selectStmt.executeQuery()) {
                    if (rs.next()) {

                        int likesId = rs.getInt("id");
                        Round round = getAssociatedRound(rs.getInt("round_id"));
                        Painting painting = getAssociatedPainting(rs.getInt("painting_id"));

                        // Update Likes number
                        updateStmt.setInt(1, likes.getId());
                        updateStmt.executeUpdate();

                        selectStmt.setInt(1, likes.getId());
                        try (ResultSet result = selectStmt.executeQuery()) {
                            if (result.next()) {
                                // Create a new Likes instance with updated values
                                updatedLikes = new Likes(likesId, result.getInt("number"), round, painting);
                            }
                        }

                    }
                }

                connection.commit();

            } catch (SQLException e) {
                e.printStackTrace();
                try {
                    if (connection != null) {
                        connection.rollback();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } finally {
                try {
                    if (connection != null) {
                        connection.setAutoCommit(true);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return updatedLikes;
    }

    /**
     * Retrieves whether a user has liked a specific painting.
     * 
     * @param user_id     The ID of the user.
     * @param painting_id The ID of the painting.
     * @param round_id    The ID of the round.
     * @return true if the user has liked the painting, false otherwise.
     */
    public Boolean isPaintingLiked(int user_id, int painting_id, int round_id) {
        boolean isLiked = false;

        try (Connection connection = dataSource.getConnection()) {
            String selectSQL = """
                    SELECT *
                    FROM user_likes ul
                    JOIN likes l on ul.likes_id = l.id
                    WHERE ul.user_id = ? AND l.painting_id = ? AND l.round_id = ?;
                    """;
            try (PreparedStatement getStmt = connection.prepareStatement(selectSQL)) {
                getStmt.setInt(1, user_id);
                getStmt.setInt(2, painting_id);
                getStmt.setInt(3, round_id);

                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        isLiked = true;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return isLiked;
    }

    /**
     * Checks if a user has liked any painting for a specific round.
     * 
     * @param user_id  The ID of the user.
     * @param round_id The ID of the round.
     * @return true if the user has liked a painting for the given round, false
     *         otherwise.
     */
    public Boolean hasUserLikedPaintingForRound(int user_id, int round_id) {
        boolean isLiked = false;

        try (Connection connection = dataSource.getConnection()) {
            String selectSQL = """
                    SELECT *
                    FROM user_likes ul
                    JOIN likes l on ul.likes_id = l.id
                    WHERE ul.user_id = ? AND l.round_id = ?;
                    """;
            try (PreparedStatement getStmt = connection.prepareStatement(selectSQL)) {
                getStmt.setInt(1, user_id);
                getStmt.setInt(2, round_id);

                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        isLiked = true;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isLiked;
    }

}
