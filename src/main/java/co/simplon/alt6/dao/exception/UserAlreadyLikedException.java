package co.simplon.alt6.dao.exception;

public class UserAlreadyLikedException extends RuntimeException {
    public UserAlreadyLikedException(String message) {
        super(message);
    }
}
