package co.simplon.alt6.dao.exception;

public class UserAlreadyPostedException extends RuntimeException {
    public UserAlreadyPostedException(String message) {
        super(message);
    }
}
