package co.simplon.alt6.dao.interfaces;

import co.simplon.alt6.entities.Likes;

import java.util.List;

public interface LikesDao extends Dao<Likes> {
    List<Likes> getLikesWithRounds();

    List<Likes> getLikesByRoundId(int id);

    Likes likesPictures(Likes likes);
    Likes unlikesPictures(Likes likes);

}
