package co.simplon.alt6.dao.interfaces;

import java.time.LocalDate;
import java.util.List;

import co.simplon.alt6.entities.Contest;
import co.simplon.alt6.entities.Round;

public interface ContestDao extends Dao<Contest> {
    List<Round> getAllRoundByContestId(int id);

    LocalDate getLastEndDate();

    // Le gagnant
    // Month - 2
    Contest getFinalRound();

    // 2 Derniers
    // Month - 1
    Contest getPreviousRound();

    // Qualification ( tout les dessins )
    // Actual Month
    Contest getActualRound();

    // Prochain Theme
    // Month + 1
    Contest getNextRound();
}