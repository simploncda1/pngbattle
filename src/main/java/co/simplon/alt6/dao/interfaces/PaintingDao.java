package co.simplon.alt6.dao.interfaces;

import java.util.List;

import co.simplon.alt6.entities.Painting;

public interface PaintingDao extends Dao<Painting> {

    List<Painting> getPaitingByUserId(int id_user);

    Boolean isUserAlreadyPosted(int round_id, int user_id);

}