package co.simplon.alt6.dao.interfaces;

import co.simplon.alt6.entities.Contest;
import co.simplon.alt6.entities.Round;

public interface RoundDao extends Dao<Round> {
    Contest getAssociatedContest(int id);
}