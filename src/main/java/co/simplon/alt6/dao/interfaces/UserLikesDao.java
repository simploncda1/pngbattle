package co.simplon.alt6.dao.interfaces;

import java.util.List;

import co.simplon.alt6.entities.UserLikes;

public interface UserLikesDao extends Dao<UserLikes> {
    List<UserLikes> getLikesByUserId(int user_id);
    Boolean deleteUserLikesByIdAndLikes(int user_id, int likes_id);
}