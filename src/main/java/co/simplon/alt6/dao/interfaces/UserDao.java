package co.simplon.alt6.dao.interfaces;

import java.util.Optional;

import co.simplon.alt6.entities.User;

public interface UserDao extends Dao<User> {
    Optional<User> getByEmail(String email);
}