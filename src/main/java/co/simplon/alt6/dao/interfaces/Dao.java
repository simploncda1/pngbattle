package co.simplon.alt6.dao.interfaces;

import java.util.List;

public interface Dao<E> {
    boolean add(E entity);
    List<E> getAll();
    E getById(int id);
    boolean update(E entity);
    boolean delete(int id);
}
