package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.dao.interfaces.UserLikesDao;
import co.simplon.alt6.entities.UserLikes;

@Repository
public class UserLikesDaoImpl extends AbstractDao<UserLikes> implements UserLikesDao {

    @Autowired
    private DataSource dataSource;

    public UserLikesDaoImpl() {
        super(
                "INSERT INTO user_likes (user_id,likes_id) VALUES (?,?)",
                "SELECT * FROM user_likes",
                "SELECT * FROM user_likes WHERE id=?",
                "UPDATE user_likes SET user_id=?,likes_id? WHERE id=?",
                "DELETE FROM user_likes WHERE id=?");
    }

    @Override
    protected UserLikes sqlToEntity(ResultSet rs) throws SQLException {
        return new UserLikes(
                rs.getInt("user_id"),
                rs.getInt("likes_id"));
    }

    @Override
    protected void setEntityId(UserLikes entity, int id) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'setEntityId'");
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, UserLikes entity) throws SQLException {
        stmt.setInt(1, entity.getUserId());
        stmt.setInt(2, entity.getLikesId());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, UserLikes entity) throws SQLException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'entityBindValuesWithId'");
    }

    /**
     * Retrieves a list of UserLikes objects associated with a specific user ID.
     * 
     * @param user_id The ID of the user.
     * @return A list of UserLikes objects associated with the user ID.
     */
    @Override
    public List<UserLikes> getLikesByUserId(int user_id) {
        List<UserLikes> userLikesList = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            String SQL = "SELECT * FROM user_likes WHERE user_id = ?";
            try (PreparedStatement getStmt = connection.prepareStatement(SQL)) {
                getStmt.setInt(1, user_id);
                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        UserLikes userLikes = new UserLikes(
                                rs.getInt("user_id"),
                                rs.getInt("likes_id"));
                        userLikesList.add(userLikes);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userLikesList;
    }

    /**
     * Supprime les likes d'un utilisateur en fonction de l'ID de l'utilisateur et
     * de l'ID des likes.
     * 
     * @param user_id  L'ID de l'utilisateur pour lequel supprimer les likes.
     * @param likes_id L'ID des likes à supprimer.
     * @return true si les likes ont été supprimés avec succès, sinon false.
     */
    @Override
    public Boolean deleteUserLikesByIdAndLikes(int user_id, int likes_id) {
        try (Connection connection = dataSource.getConnection()) {
            String SQL = "DELETE FROM user_likes WHERE user_id = ? AND likes_id = ?";
            try (PreparedStatement getStmt = connection.prepareStatement(SQL)) {
                getStmt.setInt(1, user_id);
                getStmt.setInt(2, likes_id);

                int rowsAffected = getStmt.executeUpdate();
                // Vérifier si des lignes ont été supprimées avec succès
                return rowsAffected > 0;

            } catch (Exception e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
