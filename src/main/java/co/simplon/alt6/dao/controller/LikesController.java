package co.simplon.alt6.dao.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt6.dao.business.LikesBusiness;
import co.simplon.alt6.dao.exception.UserAlreadyLikedException;
import co.simplon.alt6.entities.Likes;
import co.simplon.alt6.entities.User;

@CrossOrigin
@RestController
@RequestMapping("/api/likes")
public class LikesController {

    @Autowired
    private LikesBusiness likesBusiness;

    @GetMapping
    public List<Likes> getAll() {
        return likesBusiness.getLikesWithRounds();
    }

    // Avec l'id d'un round renvoie toutes les paintings
    @GetMapping("/{id}/round")
    public List<Likes> getOne(@PathVariable int id) {
        return likesBusiness.getLikesByRoundId(id);
    }

    @PostMapping("{id}/like/{round_id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Likes like(@AuthenticationPrincipal User user, @PathVariable int id, @PathVariable int round_id) {
        try {
            return likesBusiness.likesPictures(user, id, round_id);
        } catch (UserAlreadyLikedException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User already liked");
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{painting_id}/painting/{round_id}")
    public Boolean isPaintingLiked(@AuthenticationPrincipal User user, @PathVariable int painting_id,
            @PathVariable int round_id) {
        return likesBusiness.isPaintingLiked(user, painting_id, round_id);
    }

}
