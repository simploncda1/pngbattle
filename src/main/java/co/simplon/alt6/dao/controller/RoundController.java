package co.simplon.alt6.dao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt6.dao.RoundDaoImpl;
import co.simplon.alt6.entities.Round;

@RestController
@RequestMapping("/api/round")
public class RoundController {

    @Autowired
    private RoundDaoImpl repo;

    @GetMapping
    public List<Round> getAll() {
        return repo.getAll();
    }

    @GetMapping("/{id}")
    public Round getOne(@PathVariable int id) {
        return repo.getById(id);
    }

    // imo pas besoin car on post les Rounds via le Contest

    // @PostMapping
    // @ResponseStatus(HttpStatus.CREATED)
    // public Round add(@RequestBody Round round) {
    // repo.add(round);
    // return round;
    // }

}
