package co.simplon.alt6.dao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt6.dao.business.ContestBusiness;
import co.simplon.alt6.entities.Contest;

@RestController
@RequestMapping("/api/contest")
public class ContestController {

    @Autowired
    private ContestBusiness contestBusiness;

    @GetMapping
    public List<Contest> getAll() {
        return contestBusiness.getAll();
    }

    // Celui avec le gagnant donc mois - 2
    @GetMapping("/final")
    public Contest getFinalRound() {
        return contestBusiness.getFinalRound();
    }

    // Celui avec les 2 finalistes donc mois - 1
    @GetMapping("/previous")
    public Contest getPreviousRound() {
        return contestBusiness.getPreviousRound();
    }

    // Celui avec toutes les participations
    @GetMapping("/actual")
    public Contest getActualRound() {
        return contestBusiness.getActualRound();
    }

    // Affiche le prochain Contest
    @GetMapping("/next")
    public Contest getNextRound() {
        return contestBusiness.getNextRound();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Contest> getContestWithRounds(@PathVariable int id) {
        Contest contest = contestBusiness.getContestWithRounds(id);
        return new ResponseEntity<>(contest, HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contest add(@RequestBody Contest contest) {
        // contestBusiness.createContestWithRounds(contest);
        contestBusiness.add(contest);
        return contest;
    }

}
