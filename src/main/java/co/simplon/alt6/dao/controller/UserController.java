package co.simplon.alt6.dao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt6.dao.business.UserBusiness;
import co.simplon.alt6.entities.User;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserBusiness userBusiness;

    // Profil public
    @GetMapping("/{id}")
    public User getOne(@PathVariable int id) {
        return userBusiness.getOne(id);
    }

    @GetMapping()
    public List<User> getAll() {
        return userBusiness.getAll();
    }

    // update ?

}
