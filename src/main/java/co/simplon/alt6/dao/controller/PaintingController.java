package co.simplon.alt6.dao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

import co.simplon.alt6.dao.business.PaintingBusiness;
import co.simplon.alt6.dao.exception.UserAlreadyPostedException;
import co.simplon.alt6.entities.Painting;
import co.simplon.alt6.entities.User;

@CrossOrigin
@RestController
@RequestMapping("/api/painting")
public class PaintingController {

    @Autowired
    private PaintingBusiness paintingBusiness;

    @GetMapping
    public List<Painting> getAll() {
        return paintingBusiness.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Painting> getOne(@PathVariable int id) {
        Painting painting = paintingBusiness.getOne(id);
        return new ResponseEntity<>(painting, HttpStatus.OK);
    }

    @PostMapping("/round")
    @ResponseStatus(HttpStatus.CREATED)
    public Painting add(@AuthenticationPrincipal User user, @RequestBody Painting painting) {
        try {
            paintingBusiness.postPainting(user, painting);
        } catch (UserAlreadyPostedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "User already posted");
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return painting;
    }

    // Pas d'upate car triche possible
    // Update all except Date and Picture

}
