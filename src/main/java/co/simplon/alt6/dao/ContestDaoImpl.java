package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.dao.interfaces.ContestDao;
import co.simplon.alt6.entities.Contest;
import co.simplon.alt6.entities.Likes;
import co.simplon.alt6.entities.Painting;
import co.simplon.alt6.entities.Round;
import co.simplon.alt6.entities.User;

@Repository
public class ContestDaoImpl extends AbstractDao<Contest> implements ContestDao {

    @Autowired
    private DataSource dataSource;

    public ContestDaoImpl() {
        super(
                "INSERT INTO contest(length,title) VALUES(?,?);",
                "SELECT * FROM contest",
                "SELECT * FROM contest WHERE id=?",
                "UPDATE contest SET length=?,title=? WHERE id=?",
                // "DELETE FROM contest WHERE id=?"
                // "DELETE c, r FROM contest c LEFT JOIN round r ON c.id = r.contest_id WHERE
                // c.id = ?;"
                "DELETE FROM round WHERE id = ? UPDATE likes SET round_id = NULL WHERE round_id = ?;");
    }

    @Override
    protected Contest sqlToEntity(ResultSet rs) throws SQLException {
        return new Contest(
                rs.getInt("id"),
                rs.getDate("length").toLocalDate(),
                rs.getString("title"));
    }

    @Override
    protected void setEntityId(Contest entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Contest entity) throws SQLException {
        // stmt.setDate(1, java.sql.Date.valueOf(entity.getLength()));
        // stmt.setDate(1, java.sql.Date.valueOf(LocalDate.now()));

        // Defini la date debut au premier jours du prochain mois
        // En fonction de la derniere date dans la BDD
        // Si y a déjà 2000-01-01 la date sera 2000-02-01
        LocalDate lastEndDate = getLastEndDate();
        LocalDate nextStartDate = lastEndDate != null ? lastEndDate.plusMonths(1).withDayOfMonth(1)
                : LocalDate.now().withDayOfMonth(1);

        stmt.setDate(1, java.sql.Date.valueOf(nextStartDate));

        // Defini la date de debut au premier jours du prochain mois
        // stmt.setDate(1,
        // java.sql.Date.valueOf(LocalDate.now().withDayOfMonth(1).plusMonths(1)));

        stmt.setString(2, entity.getTitle());
    }

    /**
     * Récupère la date de fin maximale de la table "contest".
     *
     * Utilise une connexion à la base de données pour exécuter une requête SQL
     * visant à obtenir la date maximale de la colonne "length" dans la table
     * "contest".
     *
     * @return La date de fin maximale sous forme de LocalDate si un résultat est
     *         trouvé,
     *         sinon renvoie null.
     */
    public LocalDate getLastEndDate() {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement("SELECT MAX(length) FROM contest");
                ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                return resultSet.getDate(1).toLocalDate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Contest entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(3, entity.getId());
    }

    /**
     * Pour l'instant cela supprime un contest, puis les rounds et met les likes lié
     * au round à null
     * A modifier au moment du cours SQL
     */
    @Override
    public boolean delete(int contestId) {
        try (Connection connection = dataSource.getConnection()) {

            // Suppression des likes liés au round (null)
            String updateLikesSQL = "UPDATE likes SET round_id = NULL WHERE round_id IN (SELECT id FROM round WHERE contest_id = ?)";
            try (PreparedStatement updateLikesStmt = connection.prepareStatement(updateLikesSQL)) {
                updateLikesStmt.setInt(1, contestId);
                int rowsUpdated = updateLikesStmt.executeUpdate();
                if (rowsUpdated <= 0) {
                    connection.rollback();
                    return false;
                }
            }

            // Suppression des rounds liés au contest
            String deleteRoundSQL = "DELETE FROM round WHERE contest_id = ?";
            try (PreparedStatement deleteRoundStmt = connection.prepareStatement(deleteRoundSQL)) {
                deleteRoundStmt.setInt(1, contestId);
                int rowsDeleted = deleteRoundStmt.executeUpdate();
                if (rowsDeleted <= 0) {
                    connection.rollback();
                    return false;
                }
            }

            // Enfin, supprimez le contest lui-même
            String deleteContestSQL = "DELETE FROM contest WHERE id = ?";
            try (PreparedStatement deleteContestStmt = connection.prepareStatement(deleteContestSQL)) {
                deleteContestStmt.setInt(1, contestId);
                int rowsDeleted = deleteContestStmt.executeUpdate();
                if (rowsDeleted <= 0) {
                    connection.rollback();
                    return false;
                }
            }

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Récupère tous les Rounds associés à un Contest spécifié par son ID.
     *
     * @param id L'ID du Contest pour lequel récupérer les Rounds.
     * @return Une liste d'objets Round associés au Contest.
     */
    @Override
    public List<Round> getAllRoundByContestId(int id) {
        List<Round> rounds = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            String SQL = "SELECT * FROM round WHERE contest_id = ?";
            try (PreparedStatement getStmt = connection.prepareStatement(SQL)) {
                getStmt.setInt(1, id);

                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        Round round = new Round();
                        round.setId(rs.getInt("id"));
                        round.setPhase(rs.getString("phase"));
                        round.setMaxParticipation(rs.getInt("max_participation"));

                        // Si vous avez besoin de récupérer également les détails du contest, vous
                        // pouvez le faire ici.
                        // round.setContest(...);

                        rounds.add(round);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rounds;
    }

    /**
     * Récupère le Contest et son Round précédent avec la phase 'Winner' du mois
     * précédent (mois - 2). La requête inclut des informations sur les likes, la
     * paintings associées et les users correspondants.
     *
     * @return L'objet Contest représentant le Contest et son Round 'Winner' du
     *         mois précédent,
     *         ou null si aucune donnée n'est trouvée ou en cas d'erreur SQL.
     */
    @Override
    public Contest getFinalRound() {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT c.*, r.*, l.*, p.*, u.* " +
                                "FROM contest c " +
                                "JOIN round r ON c.id = r.contest_id " +
                                "JOIN likes l ON r.id = l.round_id " +
                                "JOIN painting p ON l.painting_id = p.id " +
                                "JOIN user u ON p.user_id = u.id " +
                                "WHERE MONTH(c.length) = MONTH(CURDATE() - INTERVAL 2 MONTH) " +
                                "AND YEAR(c.length) = YEAR(CURDATE() - INTERVAL 2 MONTH) " +
                                "AND r.phase = 'Winner' " +
                                "ORDER BY l.number DESC LIMIT 1"
                );

                ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {

                Contest contest = new Contest();
                contest.setId(resultSet.getInt("c.id"));
                contest.setLength(resultSet.getDate("c.length").toLocalDate());
                contest.setTitle(resultSet.getString("c.title"));

                Round round = new Round();
                round.setId(resultSet.getInt("r.id"));
                round.setPhase(resultSet.getString("r.phase"));
                round.setMaxParticipation(resultSet.getInt("r.max_participation"));

                Painting painting = new Painting();
                painting.setId(resultSet.getInt("p.id"));
                painting.setTitle(resultSet.getString("p.title"));
                painting.setDescription(resultSet.getString("p.description"));
                painting.setPicture(resultSet.getString("p.picture"));
                painting.setDate(resultSet.getDate("p.date").toLocalDate());
                painting.setUser_id(resultSet.getInt("p.user_id"));

                Likes likes = new Likes();
                likes.setNumber(resultSet.getInt("l.number"));
                painting.setLikes(likes);

                List<Painting> paintings = new ArrayList<>();
                paintings.add(painting);

                round.setPaintings(paintings);

                List<Round> rounds = new ArrayList<>();
                rounds.add(round);

                User user = new User();
                user.setId(resultSet.getInt("u.id"));
                user.setName(resultSet.getString("u.name"));
                user.setFirst_name(resultSet.getString("u.first_name"));
                user.setNickname(resultSet.getString("u.nickname"));

                List<User> users = new ArrayList<>();
                users.add(user);

                contest.setUsers(users);

                contest.setRounds(rounds);
                return contest;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Récupère le Contest et son Round précédent avec la phase 'Finals'.
     *
     * @return L'objet Contest représentant le Contest et son Round 'Finals'
     *         précédent,
     *         ou null si aucune donnée n'est trouvée.
     */
    @Override
    public Contest getPreviousRound() {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT c.*, r.*, l.*, p.* , u.* " +
                                "FROM contest c " +
                                "JOIN round r ON c.id = r.contest_id " +
                                "JOIN likes l ON r.id = l.round_id " +
                                "JOIN painting p ON l.painting_id = p.id " +
                                "JOIN user u ON p.user_id = u.id " +
                                "WHERE MONTH(c.length) = MONTH(CURDATE() - INTERVAL 1 MONTH) " +
                                "AND YEAR(c.length) = YEAR(CURDATE() - INTERVAL 1 MONTH) " +
                                "AND r.phase = 'Finals' " +
                                "ORDER BY l.number DESC LIMIT 2"
                                );
                ResultSet resultSet = statement.executeQuery()) {

            Contest contest = null;

            List<Round> rounds = new ArrayList<>();
            Round round = new Round();
            List<User> users = new ArrayList<>();
            List<Painting> paintings = new ArrayList<>();

            while (resultSet.next()) {
                if (contest == null) {
                    contest = new Contest();
                    contest.setId(resultSet.getInt("c.id"));
                    contest.setLength(resultSet.getDate("c.length").toLocalDate());
                    contest.setTitle(resultSet.getString("c.title"));
                }

                round.setId(resultSet.getInt("r.id"));
                round.setPhase(resultSet.getString("r.phase"));
                round.setMaxParticipation(resultSet.getInt("r.max_participation"));

                Painting painting = new Painting();
                painting.setId(resultSet.getInt("p.id"));
                painting.setTitle(resultSet.getString("p.title"));
                painting.setDescription(resultSet.getString("p.description"));
                painting.setPicture(resultSet.getString("p.picture"));
                painting.setDate(resultSet.getDate("p.date").toLocalDate());
                painting.setUser_id(resultSet.getInt("p.user_id"));

                Likes likes = new Likes();
                likes.setNumber(resultSet.getInt("l.number"));
                painting.setLikes(likes);

                paintings.add(painting);
                round.setPaintings(paintings);

                User user = new User();
                user.setId(resultSet.getInt("u.id"));
                user.setName(resultSet.getString("u.name"));
                user.setFirst_name(resultSet.getString("u.first_name"));
                user.setNickname(resultSet.getString("u.nickname"));

                users.add(user);

            }
            rounds.add(round);
            contest.setUsers(users);
            contest.setRounds(rounds);

            return contest;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Cette méthode récupère le Round actuel d'un Contest en cours.
     * 
     * Le Round actuel est défini comme le dernier Round de la phase de
     * Qualification
     * pour le Contest dont la durée (length) se situe dans le même mois et la
     * même année que la date actuelle.
     * 
     * Si aucun Round de qualification n'est trouvé pour ce critère, la méthode
     * renvoie null.
     *
     * @return Un objet Contest représentant le Contest en cours avec son dernier
     *         Round de qualification,
     *         ou null s'il n'y a pas de Contest en cours.
     */
    @Override
    // MARCHE PAS SI PAS DE TABLEAU !!!
    // TODO:
    // FIXME:
    public Contest getActualRound() {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT c.*, r.*, l.*, p.* , u.* " +
                                "FROM contest c " +
                                "JOIN round r ON c.id = r.contest_id " +
                                "JOIN likes l ON r.id = l.round_id " +
                                "JOIN painting p ON l.painting_id = p.id " +
                                "JOIN user u ON p.user_id = u.id " + "WHERE MONTH(c.length) = MONTH(CURDATE()) " +
                                "AND YEAR(c.length) = YEAR(CURDATE()) " +
                                "AND r.phase = 'Qualification' " +
                                "ORDER BY l.number DESC"
                // +
                // "LIMIT 1");
                );
                ResultSet resultSet = statement.executeQuery()) {

            Contest contest = null;

            List<Round> rounds = new ArrayList<>();
            Round round = new Round();
            List<User> users = new ArrayList<>();
            List<Painting> paintings = new ArrayList<>();

            while (resultSet.next()) {
                if (contest == null) {
                    contest = new Contest();
                    contest.setId(resultSet.getInt("c.id"));
                    contest.setLength(resultSet.getDate("c.length").toLocalDate());
                    contest.setTitle(resultSet.getString("c.title"));
                }

                round.setId(resultSet.getInt("r.id"));
                round.setPhase(resultSet.getString("r.phase"));
                round.setMaxParticipation(resultSet.getInt("r.max_participation"));

                Painting painting = new Painting();
                painting.setId(resultSet.getInt("p.id"));
                painting.setTitle(resultSet.getString("p.title"));
                painting.setDescription(resultSet.getString("p.description"));
                painting.setPicture(resultSet.getString("p.picture"));
                painting.setDate(resultSet.getDate("p.date").toLocalDate());
                painting.setUser_id(resultSet.getInt("p.user_id"));

                Likes likes = new Likes();
                likes.setNumber(resultSet.getInt("l.number"));
                painting.setLikes(likes);

                paintings.add(painting);
                // if (!paintings.isEmpty()){
                round.setPaintings(paintings);
                // }

                User user = new User();
                user.setId(resultSet.getInt("u.id"));
                user.setName(resultSet.getString("u.name"));
                user.setFirst_name(resultSet.getString("u.first_name"));
                user.setNickname(resultSet.getString("u.nickname"));

                users.add(user);

            }
            rounds.add(round);
            contest.setUsers(users);
            contest.setRounds(rounds);

            return contest;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

        // if (resultSet.next()) {

        // Contest contest = new Contest();
        // contest.setId(resultSet.getInt("c.id"));
        // contest.setLength(resultSet.getDate("c.length").toLocalDate());
        // contest.setTitle(resultSet.getString("c.title"));

        // Round round = new Round();
        // round.setId(resultSet.getInt("r.id"));
        // round.setPhase(resultSet.getString("r.phase"));
        // round.setMaxParticipation(resultSet.getInt("r.max_participation"));

        // List<Round> rounds = new ArrayList<>();
        // rounds.add(round);

        // contest.setRounds(rounds);
        // return contest;
        // }

        // } catch (SQLException e) {
        // e.printStackTrace();
        // }

        // return null;
    }

    /**
     * Récupère le prochain Round du Contest depuis la base de données.
     * 
     * @return Le prochain Round du Contest, ou null s'il n'y en a pas ou en cas
     *         d'erreur.
     */
    @Override
    public Contest getNextRound() {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT * FROM contest WHERE length > CURDATE() ORDER BY length LIMIT 1;");

                ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {

                Contest contest = new Contest();
                contest.setId(resultSet.getInt("id"));
                contest.setLength(resultSet.getDate("length").toLocalDate());
                contest.setTitle(resultSet.getString("title"));

                return contest;
            }

        } catch (SQLException e) {
            e.printStackTrace(); // Gérez ou enregistrez l'exception de manière appropriée
        }

        // En cas de non trouvé ou d'erreur, renvoyer null ou une valeur par défaut
        return null;
    }

}