package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.dao.interfaces.UserDao;
import co.simplon.alt6.entities.User;

@Repository
public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    @Autowired
    private DataSource dataSource;

    public UserDaoImpl() {
        super(
                "INSERT INTO user (name,first_name,nickname,email,birthdate,gender,password) VALUES (?,?,?,?,?,?,?)",
                "SELECT * FROM user",
                "SELECT * FROM user WHERE id=?",
                "UPDATE user SET name=?,first_name=?,nickname=?,email=?,birthdate=?,gender=?,password=? WHERE id=?",
                "DELETE FROM user WHERE id=?");
    }

    @Override
    protected User sqlToEntity(ResultSet rs) throws SQLException {
        return new User(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getString("first_name"),
                rs.getString("nickname"),
                rs.getString("email"),
                rs.getDate("birthdate").toLocalDate(),
                rs.getString("gender"),
                rs.getString("password"));
    }

    @Override
    protected void setEntityId(User entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, User entity) throws SQLException {
        stmt.setString(1, entity.getName());
        stmt.setString(2, entity.getFirst_name());
        stmt.setString(3, entity.getNickname());
        stmt.setString(4, entity.getEmail());
        stmt.setDate(5, java.sql.Date.valueOf(entity.getBirthdate()));
        stmt.setString(6, entity.getGender());
        stmt.setString(7, entity.getPassword());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, User entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(8, entity.getId());
    }

    @Override
    public Optional<User> getByEmail(String email) {
        try (Connection connection = dataSource.getConnection()) {
            String SQL = "SELECT * FROM user WHERE email = ?";
            try (PreparedStatement getStmt = connection.prepareStatement(SQL)) {
                getStmt.setString(1, email);
                try (ResultSet rs = getStmt.executeQuery()) {
                    while (rs.next()) {
                        User user = new User(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("first_name"),
                                rs.getString("nickname"),
                                rs.getString("email"),
                                rs.getDate("birthdate").toLocalDate(),
                                rs.getString("gender"),
                                rs.getString("password"));
                        return Optional.of(user);
                    }
                } catch (Exception e) {
                }
            } catch (Exception e) {
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

}
