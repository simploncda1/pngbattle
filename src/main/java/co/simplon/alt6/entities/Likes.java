package co.simplon.alt6.entities;

public class Likes {

    private int id;
    private int number;

    private Round round;
    private Painting painting;

    public Likes(int id, int number, Round round, Painting painting) {
        this.id = id;
        this.number = number;
        this.round = round;
        this.painting = painting;
    }

    public Likes(int number, Round round, Painting painting) {
        this.number = number;
        this.round = round;
        this.painting = painting;
    }

    public Likes() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public Painting getPainting() {
        return painting;
    }

    public void setPainting(Painting painting) {
        this.painting = painting;
    }

}
