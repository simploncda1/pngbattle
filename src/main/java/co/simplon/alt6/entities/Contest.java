package co.simplon.alt6.entities;

import java.time.LocalDate;
import java.util.List;

public class Contest {

    private int id;
    private LocalDate length;
    private String title;
    private List<Round> rounds;
    private List<User> users;

    public Contest() {
    }

    public Contest(String title) {
        this.title = title;
    }

    public Contest(int int1) {
    }

    public Contest(LocalDate length, String title) {
        this.length = length;
        this.title = title;
    }

    public Contest(int id, LocalDate length, String title) {
        this.id = id;
        this.length = length;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getLength() {
        return length;
    }

    public void setLength(LocalDate length) {
        this.length = length;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
