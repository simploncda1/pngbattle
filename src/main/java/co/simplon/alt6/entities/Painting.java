package co.simplon.alt6.entities;

import java.time.LocalDate;

public class Painting {

    private int id;
    private String title;
    private String description;
    private String picture;
    private LocalDate date;
    private int user_id;
    private Likes likes;

    public Painting() {
    }

    public Painting(String title, String description, String picture, LocalDate date, int user_id) {
        this.title = title;
        this.description = description;
        this.picture = picture;
        this.date = date;
        this.user_id = user_id;
    }

    public Painting(int id, String title, String description, String picture, LocalDate date, int user_id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.picture = picture;
        this.date = date;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }
}
