package co.simplon.alt6.entities;

public class UserLikes {

    private int userId;
    private int likesId;

    public UserLikes(int userId, int likesId) {
        this.userId = userId;
        this.likesId = likesId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getLikesId() {
        return likesId;
    }

    public void setLikesId(int likesId) {
        this.likesId = likesId;
    }
}
