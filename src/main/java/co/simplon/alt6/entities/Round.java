package co.simplon.alt6.entities;

import java.util.List;

public class Round {

    private int id;
    private String phase;
    private int maxParticipation;
    private Contest contest;
    private List<Painting> paintings;

    public Round() {
    }

    public Round(int id, String phase, int maxParticipation) {
        this.id = id;
        this.phase = phase;
        this.maxParticipation = maxParticipation;
    }

    public Round(String phase, int maxParticipation, Contest contest) {
        this.phase = phase;
        this.maxParticipation = maxParticipation;
        this.contest = contest;
    }

    public Round(int id, String phase, int maxParticipation, Contest contest) {
        this.id = id;
        this.phase = phase;
        this.maxParticipation = maxParticipation;
        this.contest = contest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public int getMaxParticipation() {
        return maxParticipation;
    }

    public void setMaxParticipation(int maxParticipation) {
        this.maxParticipation = maxParticipation;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public List<Painting> getPaintings() {
        return paintings;
    }

    public void setPaintings(List<Painting> painting) {
        this.paintings = painting;
    }

}
