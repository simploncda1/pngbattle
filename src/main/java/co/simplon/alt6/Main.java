package co.simplon.alt6;

import co.simplon.alt6.dao.interfaces.UserDao;
import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {

    @Autowired
    private UserDao userDao;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @PostConstruct
    public void run() {
        System.out.println("Hello world!");
        System.out.println(userDao.getAll());
    }


}