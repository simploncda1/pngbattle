-- Active: 1673947616242@@127.0.0.1@3306@alt6_pngbattle

-- Création d'un déclencheur (trigger) pour insérer des Rounds prédéfinis lorsqu'un nouveau Contest est ajouté.
DELIMITER $$

CREATE OR REPLACE TRIGGER before_contest_insert
AFTER INSERT ON contest
FOR EACH ROW

BEGIN
    -- Déclaration des Rounds prédéfinis
    DECLARE round1_phase VARCHAR(255) DEFAULT 'Qualification';
    DECLARE round1_max_participation INT DEFAULT 50;
    DECLARE round2_phase VARCHAR(255) DEFAULT 'Finals';
    DECLARE round2_max_participation INT DEFAULT 2;
    DECLARE round3_phase VARCHAR(255) DEFAULT 'Winner';
    DECLARE round3_max_participation INT DEFAULT 1;

    -- Insertion du Round de Qualification
    INSERT INTO round (phase, max_participation, contest_id)
    VALUES (round1_phase, round1_max_participation, NEW.id);

    -- Insertion du Round des Finals
    INSERT INTO round (phase, max_participation, contest_id)
    VALUES (round2_phase, round2_max_participation, NEW.id);

    -- Insertion du Round du Winner
    INSERT INTO round (phase, max_participation, contest_id)
    VALUES (round3_phase, round3_max_participation, NEW.id);
END $$

DELIMITER ;