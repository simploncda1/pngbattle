-- Active: 1673947616242@@127.0.0.1@3306@alt6_pngbattle

INSERT INTO user 
(name, first_name, nickname, email, birthdate, gender, password)
VALUES 
('Doe', 'John', 'johndoe', 'john@example.com', '1990-01-15', 'Male', 'password123'),
('Smith','Alice','alice','alice@example.com','1985-03-20','Female','p@ssw0rd'),
('Rubin', 'Laura','H','laurahrubin@gmail.com','1980-06-03','Artist','A2BetterThan2B'),
('Ling', 'Wang','Wlop','WangLing@gmail.com','1989-09-26','Artist','CMoiQuiDessinLeMieux'),
('Lau', 'Stanley','Artgerm','StanleyLau@gmail.com','1960-06-03','Comics','J''aimeLesCopicsPlusQueToutAuMonde'),
('Hildans','Alexandre','Eksandr','AlexandreHildans@gmail.com','1900-01-01','Roux','$2a$12$701lfeW0o97j4X3doCRQvO5GUHLIQuezuepXoD/4RJISS1dyf15qy'),
('Yusuke', 'Kozaki', 'x', 'YusukeKozaki@gmail.com' ,'1900-01-01', 'weeb', 'TarjaBFF'),
# Password is test
('test','test','test','test@test.com','0001-01-01','bot','$2a$12$701lfeW0o97j4X3doCRQvO5GUHLIQuezuepXoD/4RJISS1dyf15qy');

INSERT INTO painting 
(title, description, picture, date, user_id)
VALUES
('Landscape','A beautiful landscape painting','landscape.jpg','2023-04-10',1),
('Portrait','A stunning portrait','portrait.jpg','2023-04-12',2),
--
('Hope','c bo hein','LauraRubin/butterfly.png','2023-11-15',3),
('wlopDemi','c pa wlop mais soon','WangLing/bleu.png','2023-11-16',4),
('MLP','c pas lui nn plus','StanleyLau/rose.png','2019_08_05',5),
--
('Le Roi Arthur','si c''est vraiment lui tkt', 'AlexandreHildans/alexandre-hildans-sabre-foi.jpg','2023-11-01',6),
('Tattoo','enlève tes chaussettes','AlexandreHildans/alexandre-hildans-dtiys-joslauart.jpg','2023-01-01',6),
('畜生道','Chikushodô','AlexandreHildans/alexandre-hildans-pain-rikudo.jpg','2023-01-01',6),
('Tharja','Turunen','AlexandreHildans/tharja.png','2023-01-01',6),
--
('Cordelia','Tiamo','KozakiYusuke/cordelia.png','2023-01-01',7),
('Lucina','Marth','KozakiYusuke/lucina.png','2023-01-01',7),
('Severa','Tiamo','KozakiYusuke/severa.png','2023-01-01',7),
('Sumia','Pegaz','KozakiYusuke/sumia.png','2023-01-01',7),
('Zelcher','Drake','KozakiYusuke/tiamo.png','2023-01-01',7)
;

INSERT INTO contest 
(length, title)
VALUES 
(CASE WHEN MONTH(NOW()) = 1 THEN CONCAT(YEAR(NOW()) - 1, '-11-01') 
      WHEN MONTH(NOW()) = 2 THEN CONCAT(YEAR(NOW()) - 1, '-12-01') 
      ELSE DATE_SUB(NOW(), INTERVAL 2 MONTH)
    END,'Butterfly'), -- Le winner
(CASE WHEN MONTH(NOW()) = 1 THEN CONCAT(YEAR(NOW()) - 1, '-12-01') 
      ELSE DATE_SUB(NOW(), INTERVAL 1 MONTH)
    END,'Aquarelle'), -- La finale
(DATE_FORMAT(NOW(), '%Y-%m-01'), 'Fire Emblem'), -- Contest en cours
(CASE WHEN MONTH(NOW()) = 12 THEN CONCAT(YEAR(NOW()) + 1, '-01-01') 
      ELSE DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-01'), INTERVAL 1 MONTH)
    END, 'Le Prochain Theme');

INSERT INTO likes 
(number, round_id, painting_id)
VALUES 
(100, 1, 1),
(75, 1, 2),
(69420, 3, 3), # 3 = Winner
(100, 5, 4),
(200, 5, 5), # 5 = Finaliste
(10, 5, 6), 
(10, 5, 7), 
(10, 5, 8), 
(420, 7, 9), # 7 = qualification
(400, 7, 10),
(321, 7, 11),
(3, 7, 12),
(212, 7, 13),
(400, 7, 14);



INSERT INTO user_likes (user_id, likes_id) VALUES (1, 1), (2, 2);