-- Active: 1673947616242@@127.0.0.1@3306@alt6_pngbattle

DROP TABLE IF EXISTS user_likes;

DROP TABLE IF EXISTS likes;

DROP TABLE IF EXISTS round;

DROP TABLE IF EXISTS contest;

DROP TABLE IF EXISTS painting;

DROP TABLE IF EXISTS user;

CREATE TABLE
    user(
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255),
        first_name VARCHAR(255),
        nickname VARCHAR(255),
        email VARCHAR(255),
        birthdate DATE,
        gender VARCHAR(255),
        password VARCHAR(255)
    );

CREATE TABLE
    painting(
        id INT PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255),
        description VARCHAR(255),
        picture VARCHAR(255),
        date DATE,
        user_id INT,
        Foreign Key (user_id) REFERENCES user(id) ON DELETE CASCADE
    );

CREATE TABLE
    contest(
        id INT PRIMARY KEY AUTO_INCREMENT,
        length DATE,
        title VARCHAR(255)
    );

CREATE TABLE
    round(
        id INT PRIMARY KEY AUTO_INCREMENT,
        phase VARCHAR(255),
        max_participation INT,
        contest_id INT,
        FOREIGN KEY (contest_id) REFERENCES contest(id) ON DELETE CASCADE
    );

CREATE TABLE
    likes(
        id INT PRIMARY KEY AUTO_INCREMENT,
        number INT,
        round_id INT,
        Foreign Key (round_id) REFERENCES round(id),
        painting_id INT,
        Foreign Key (painting_id) REFERENCES painting(id) ON DELETE CASCADE
    );

CREATE TABLE
    user_likes (
        user_id INT,
        likes_id INT,
        PRIMARY KEY (user_id, likes_id),
        FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE,
        FOREIGN KEY (likes_id) REFERENCES likes(id) ON DELETE CASCADE
    );

INSERT INTO
    user (
        name,
        first_name,
        nickname,
        email,
        birthdate,
        gender,
        password
    )
VALUES (
        'Doe',
        'John',
        'johndoe',
        'john@example.com',
        '1990-01-15',
        'Male',
        'password123'
    ), (
        'Smith',
        'Alice',
        'alice',
        'alice@example.com',
        '1985-03-20',
        'Female',
        'p@ssw0rd'
    );

INSERT INTO
    painting (
        title,
        description,
        picture,
        date,
        user_id
    )
VALUES (
        'Landscape',
        'A beautiful landscape painting',
        'landscape.jpg',
        '2023-04-10',
        1
    ), (
        'Portrait',
        'A stunning portrait',
        'portrait.jpg',
        '2023-04-12',
        2
    );

INSERT INTO
    contest (length, title)
VALUES (
        '2023-04-10',
        'Art Competition 1'
    ), (
        '2023-04-15',
        'Art Competition 2'
    );

INSERT INTO
    round (
        phase,
        max_participation,
        contest_id
    )
VALUES ('Qualification', 50, 1), ('Finals', 20, 1);

INSERT INTO
    likes (number, round_id, painting_id)
VALUES (100, 1, 1), (75, 1, 2);

INSERT INTO user_likes (user_id, likes_id) VALUES (1, 1), (2, 2);