package co.simplon.alt6.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import co.simplon.alt6.entities.Likes;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class LikesDaoImplTest {

    @Autowired
    private LikesDaoImpl dao;
    @Autowired
    private RoundDaoImpl round;
    @Autowired
    private PaintingDaoImpl painting;

    @BeforeEach
    void setUp() {
        // dao = new LikesDaoImpl();
        try {
            ScriptRunner runner = new ScriptRunner(Connector.getConnection());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAdd() {
        Likes toPersist = new Likes(69420, round.getById(1), painting.getById(1));
        assertTrue(dao.add(toPersist));
        assertNotNull(toPersist.getId());
        assertEquals(3, toPersist.getId());
    }

    @Test
    void testDelete() {
        assertTrue(dao.delete(1));
    }

    @Test
    void testDeleteNoMatchingId() {
        assertFalse(dao.delete(100));
    }

    @Test
    void testGetAll() {
        List<Likes> result = dao.getAll();

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getId());
        assertEquals(100, result.get(0).getNumber());
        assertEquals(1, result.get(0).getRound().getId());
        assertEquals(1, result.get(0).getPainting().getId());

        assertEquals(2, result.get(1).getId());
        assertEquals(75, result.get(1).getNumber());
        assertEquals(1, result.get(1).getRound().getId());
        assertEquals(2, result.get(1).getPainting().getId());
    }

    @Test
    void testGetById() {
        Likes result = dao.getById(1);

        assertNotNull(result);

        assertEquals(1, result.getId());
        assertEquals(100, result.getNumber());
        assertEquals(1, result.getRound().getId());
        assertEquals(1, result.getPainting().getId());
    }

    @Test
    void testGetByIdNoResult() {
        Likes result = dao.getById(100);
        assertNull(result);
    }

    @Test
    void testUpdate() {
        Likes toUpdate = new Likes(1, 69420, round.getById(1), painting.getById(1));
        // Likes toUpdate = new Likes(1, 69420, 1, 1);
        assertTrue(dao.update(toUpdate));
    }

    @Test
    void testUpdateNoMatchingId() {
        Likes toUpdate = new Likes(69420, round.getById(1), painting.getById(1));
        // Likes toUpdate = new Likes(100, 69420, 1, 1);
        assertFalse(dao.update(toUpdate));
    }

}
