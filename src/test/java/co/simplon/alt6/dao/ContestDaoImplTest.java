package co.simplon.alt6.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import co.simplon.alt6.entities.Contest;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class ContestDaoImplTest {

    @Autowired
    private ContestDaoImpl dao;

    @BeforeEach
    void setUp() {
        // dao = new ContestDaoImpl();
        try {
            ScriptRunner runner = new ScriptRunner(Connector.getConnection());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAdd() {
        Contest toPersist = new Contest(LocalDate.now(), "test");
        assertTrue(dao.add(toPersist));
        assertNotNull(toPersist.getId());
        assertEquals(3, toPersist.getId());
    }

    @Test
    void testDelete() {
        assertTrue(dao.delete(1));
    }

    @Test
    void testDeleteNoMatchingId() {
        assertFalse(dao.delete(100));
    }

    @Test
    void testGetAll() {
        List<Contest> result = dao.getAll();

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getId());
        assertEquals(LocalDate.of(2023, 4, 10), result.get(0).getLength());
        assertEquals("Art Competition 1", result.get(0).getTitle());

        assertEquals(2, result.get(1).getId());
        assertEquals(LocalDate.of(2023, 4, 15), result.get(1).getLength());
        assertEquals("Art Competition 2", result.get(1).getTitle());
    }

    @Test
    void testGetById() {
        Contest result = dao.getById(1);

        assertNotNull(result);

        assertEquals(1, result.getId());
        assertEquals(LocalDate.of(2023, 4, 10), result.getLength());
        assertEquals("Art Competition 1", result.getTitle());
    }

    @Test
    void testGetByIdNoResult() {
        Contest result = dao.getById(100);
        assertNull(result);
    }

    @Test
    void testUpdate() {
        Contest toUpdate = new Contest(1, LocalDate.now(), "test");
        assertTrue(dao.update(toUpdate));
    }

    @Test
    void testUpdateNoMatchingId() {
        Contest toUpdate = new Contest(100, LocalDate.now(), "test");
        assertFalse(dao.update(toUpdate));
    }

}
