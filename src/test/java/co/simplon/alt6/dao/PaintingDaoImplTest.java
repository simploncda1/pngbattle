package co.simplon.alt6.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import co.simplon.alt6.entities.Painting;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class PaintingDaoImplTest {

    @Autowired
    private PaintingDaoImpl dao;

    @BeforeEach
    void setUp() {
        // dao = new PaintingDaoImpl();
        try {
            ScriptRunner runner = new ScriptRunner(Connector.getConnection());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAdd() {
        Painting toPersist = new Painting("test", "test", "test", LocalDate.now(), 1);
        assertTrue(dao.add(toPersist));
        assertNotNull(toPersist.getId());
        assertEquals(3, toPersist.getId());
    }

    @Test
    void testDelete() {
        assertTrue(dao.delete(1));
    }

    @Test
    void testDeleteNoMatchingId() {
        assertFalse(dao.delete(100));
    }

    @Test
    void testGetAll() {
        List<Painting> result = dao.getAll();

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getId());
        assertEquals("Landscape", result.get(0).getTitle());
        assertEquals("A beautiful landscape painting", result.get(0).getDescription());
        assertEquals("landscape.jpg", result.get(0).getPicture());
        assertEquals(LocalDate.of(2023, 4, 10), result.get(0).getDate());
        assertEquals(1, result.get(0).getUser_id());

        assertEquals(2, result.get(1).getId());
        assertEquals("Portrait", result.get(1).getTitle());
        assertEquals("A stunning portrait", result.get(1).getDescription());
        assertEquals("portrait.jpg", result.get(1).getPicture());
        assertEquals(LocalDate.of(2023, 4, 12), result.get(1).getDate());
        assertEquals(2, result.get(1).getUser_id());
    }

    @Test
    void testGetById() {
        Painting result = dao.getById(1);

        assertNotNull(result);

        assertEquals(1, result.getId());
        assertEquals("Landscape", result.getTitle());
        assertEquals("A beautiful landscape painting", result.getDescription());
        assertEquals("landscape.jpg", result.getPicture());
        assertEquals(LocalDate.of(2023, 4, 10), result.getDate());
        assertEquals(1, result.getUser_id());
    }

    @Test
    void testGetByIdNoResult() {
        Painting result = dao.getById(100);
        assertNull(result);
    }

    @Test
    void testUpdate() {
        //UPDATE painting SET title="test",description="test",picture="test",date=null,user_id=1 WHERE id=1
        Painting toUpdate = new Painting(1,"test", "test", "test", LocalDate.now(), 2);
        assertTrue(dao.update(toUpdate));
    }

    @Test
    void testUpdateNoMatchingId() {
        Painting toUpdate = new Painting(100,"test", "test", "test", LocalDate.now(), 1);
        assertFalse(dao.update(toUpdate));
    }

}
