package co.simplon.alt6.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import co.simplon.alt6.entities.User;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class UserDaoImplTest {

    @Autowired
    private UserDaoImpl dao;

    @BeforeEach
    void setUp() {
        // dao = new UserDaoImpl();
        try {
            ScriptRunner runner = new ScriptRunner(Connector.getConnection());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAdd() {
        User toPersist = new User("John", "Doe", "Jhonny", "John.Doe@mail.com", LocalDate.now(), "man", "1234");
        assertTrue(dao.add(toPersist));
        assertNotNull(toPersist.getId());
        assertEquals(3, toPersist.getId());
    }

    @Test
    void testDelete() {
        assertTrue(dao.delete(1));
    }

    @Test
    void testDeleteNoMatchingId() {
        assertFalse(dao.delete(100));
    }

    @Test
    void testGetAll() {
        List<User> result = dao.getAll();

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getId());
        assertEquals("Doe", result.get(0).getName());
        assertEquals("John", result.get(0).getFirst_name());
        assertEquals("johndoe", result.get(0).getNickname());
        assertEquals("john@example.com", result.get(0).getEmail());
        assertEquals(LocalDate.of(1990, 1, 15), result.get(0).getBirthdate());
        assertEquals("Male", result.get(0).getGender());
        assertEquals("password123", result.get(0).getPassword());

        assertEquals(2, result.get(1).getId());
        assertEquals("Smith", result.get(1).getName());
        assertEquals("Alice", result.get(1).getFirst_name());
        assertEquals("alice", result.get(1).getNickname());
        assertEquals("alice@example.com", result.get(1).getEmail());
        assertEquals(LocalDate.of(1985, 3, 20), result.get(1).getBirthdate());
        assertEquals("Female", result.get(1).getGender());
        assertEquals("p@ssw0rd", result.get(1).getPassword());
    }

    @Test
    void testGetById() {
        User result = dao.getById(1);

        assertNotNull(result);

        assertEquals(1, result.getId());
        assertEquals("Doe", result.getName());
        assertEquals("John", result.getFirst_name());
        assertEquals("johndoe", result.getNickname());
        assertEquals("john@example.com", result.getEmail());
        assertEquals(LocalDate.of(1990, 1, 15), result.getBirthdate());
        assertEquals("Male", result.getGender());
        assertEquals("password123", result.getPassword());
    }

    @Test
    void testGetByIdNoResult() {
        User result = dao.getById(100);
        assertNull(result);
    }

    @Test
    void testUpdate() {
        User toUpdate = new User(1, "test", "test", "test", "test", LocalDate.now(), "test", "test");
        assertTrue(dao.update(toUpdate));
    }

    @Test
    void testUpdateNoMatchingId() {
        User toUpdate = new User(100, "test", "test", "test", "test", LocalDate.now(), "test", "test");
        assertFalse(dao.update(toUpdate));
    }

}
