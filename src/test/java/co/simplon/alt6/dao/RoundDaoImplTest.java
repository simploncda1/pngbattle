package co.simplon.alt6.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import co.simplon.alt6.entities.Round;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class RoundDaoImplTest {

    @Autowired
    private RoundDaoImpl dao;

    @Autowired
    private ContestDaoImpl contest;

    @BeforeEach
    void setUp() {
        // dao = new RoundDaoImpl();
        try {
            ScriptRunner runner = new ScriptRunner(Connector.getConnection());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAdd() {
        Round toPersist = new Round("Final", 2, contest.getById(1));
        assertTrue(dao.add(toPersist));
        assertNotNull(toPersist.getId());
        assertEquals(3, toPersist.getId());
    }

    @Test
    void testDelete() {
        assertTrue(dao.delete(1));
    }

    @Test
    void testDeleteNoMatchingId() {
        assertFalse(dao.delete(100));
    }

    @Test
    void testGetAll() {
        List<Round> result = dao.getAll();

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getId());
        assertEquals("Qualification", result.get(0).getPhase());
        assertEquals(50, result.get(0).getMaxParticipation());
        assertEquals(1, result.get(0).getContest().getId());

        assertEquals(2, result.get(1).getId());
        assertEquals("Finals", result.get(1).getPhase());
        assertEquals(20, result.get(1).getMaxParticipation());
        assertEquals(1, result.get(1).getContest().getId());

    }

    @Test
    void testGetById() {
        Round result = dao.getById(1);

        assertNotNull(result);

        assertEquals(1, result.getId());
        assertEquals("Qualification", result.getPhase());
        assertEquals(50, result.getMaxParticipation());
        assertEquals(1, result.getContest().getId());
    }

    @Test
    void testGetByIdNoResult() {
        Round result = dao.getById(100);
        assertNull(result);
    }

    @Test
    void testUpdate() {
        Round toUpdate = new Round(1, "Final", 2, contest.getById(1));
        assertTrue(dao.update(toUpdate));
    }

    @Test
    void testUpdateNoMatchingId() {
        Round toUpdate = new Round(100, "Final", 2, contest.getById(1));
        assertFalse(dao.update(toUpdate));
    }

}
