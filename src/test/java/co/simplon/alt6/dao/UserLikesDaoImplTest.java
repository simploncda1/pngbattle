package co.simplon.alt6.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import co.simplon.alt6.entities.UserLikes;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class UserLikesDaoImplTest {

    @Autowired
    private UserLikesDaoImpl dao;

    @BeforeEach
    void setUp() {
        // dao = new UserLikesDaoImpl();
        try {
            ScriptRunner runner = new ScriptRunner(Connector.getConnection());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testDeleteNoMatchingId() {
        assertFalse(dao.delete(100));
    }

    @Test
    void testGetAll() {
        List<UserLikes> result = dao.getAll();

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getUserId());
        assertEquals(1, result.get(0).getLikesId());

        assertEquals(2, result.get(1).getUserId());
        assertEquals(2, result.get(1).getLikesId());
    }

    @Test
    void testGetByIdNoResult() {
        UserLikes result = dao.getById(100);
        assertNull(result);
    }

}
