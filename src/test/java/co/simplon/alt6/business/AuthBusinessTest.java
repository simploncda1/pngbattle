package co.simplon.alt6.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import co.simplon.alt6.dao.UserDaoImpl;
import co.simplon.alt6.dao.business.AuthBusiness;
import co.simplon.alt6.entities.User;



@ExtendWith(MockitoExtension.class)
public class AuthBusinessTest {

    @InjectMocks
    AuthBusiness authBusiness;
    @Mock
    PasswordEncoder encoder;
    @Mock
    UserDaoImpl userRepo;

    @Test
    void testRegister() throws Exception {
        Mockito.when(encoder.encode("1234")).thenReturn("un-hash");
        User user = new User();
        user.setEmail("test@test.com");
        user.setPassword("1234");
        authBusiness.register(user);

        // Mockito.verify(encoder, Mockito.times(1)).encode("1234");
        
        assertEquals(user.getPassword(), "un-hash");
    }
    @Test
    void testRegisterFail() throws Exception {
        Mockito.when(userRepo.getByEmail("test@test.com")).thenReturn(Optional.of(new User()));
        User user = new User();
        user.setEmail("test@test.com");
        user.setPassword("1234");
        assertThrows(Exception.class, () -> authBusiness.register(user));

    }
}
