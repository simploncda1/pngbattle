# Makefile to execute SQL script

# Database connection parameters
DB_USER = simplon
DB_PASSWORD = 1234
DB_HOST = localhost
DB_PORT = 3306
DB_NAME = alt6_pngbattle

# Script file name
SQL_CREATE = ./src/main/resources/create_table.sql
SQL_TRIGGER = ./src/main/resources/trigger.sql
SQL_VALUE = ./src/main/resources/insert_value.sql


.PHONY: run

sql:
	@echo "Executing SQL script..."
	mysql -u $(DB_USER) -p$(DB_PASSWORD) -h $(DB_HOST) -P $(DB_PORT) $(DB_NAME) < $(SQL_CREATE)
	mysql -u $(DB_USER) -p$(DB_PASSWORD) -h $(DB_HOST) -P $(DB_PORT) $(DB_NAME) < $(SQL_TRIGGER)
	mysql -u $(DB_USER) -p$(DB_PASSWORD) -h $(DB_HOST) -P $(DB_PORT) $(DB_NAME) < $(SQL_VALUE)
	@echo "SQL script executed successfully"

check:
	@echo "Running tests..."
	mvn test
	@echo "Tests passed successfully"

serve:
	mvn spring-boot:run

date:
	@echo "Changement de la date..."
	timedatectl set-ntp 0
	sudo date --set="20231215 09:00"
	@echo "Date changé !"

date%:
	@echo "Date remise par défaut"
	timedatectl set-ntp 1
	@echo "Date changé !"